import java.util.ArrayList;
import java.util.List;

public class Owner {
    private String name;
    private List<Building> buildings;

    Owner(String name) {
        this.name = name;
        buildings = new ArrayList<>();
    }

    public void addBuilding(Building building) {
        buildings.add(building);
    }

    public double calculateIncome() {
        double income = 0.0;
        for (Building building : buildings) {
            income += building.getProfit();
        }

        return income;
    }

    public String getName() {
        return name;
    }

}
