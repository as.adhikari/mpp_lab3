public class Main {
    public static void main(String[] args) {
        Apartment apartment1 = new Apartment(450.00);
        Apartment apartment2 = new Apartment(780.90);
        Apartment apartment3 = new Apartment(560.50);

        Building building1 = new Building(144);
        building1.setMaintenance(300.00);
        building1.addApartment(apartment1);
        building1.addApartment(apartment2);
        building1.addApartment(apartment3);

        Apartment apartment4 = new Apartment(200.00);
        Apartment apartment5 = new Apartment(990);

        Building building2 = new Building(233, 1000);
        building2.addApartment(apartment4);
        building2.addApartment(apartment5);

        Owner owner = new Owner("MUM");
        owner.addBuilding(building1);
        owner.addBuilding(building2);

        System.out.println("Total Income of Owner " + owner.getName() + " is $" + owner.calculateIncome());
    }
}
