import java.util.ArrayList;
import java.util.List;

public class Building {
    private int buldingNum;
    private double maintenance;
    private List<Apartment> apartments;

    Building(int buildingNum) {
        buldingNum = buildingNum;
        apartments = new ArrayList<>();
    }

    Building(int buildingNum, double maintenance) {
        this.buldingNum = buildingNum;
        this.maintenance = maintenance;
        apartments = new ArrayList<>();
    }

    public void addApartment(Apartment apartment) {
        apartments.add(apartment);
    }

    public void setMaintenance(double maintenance) {
        this.maintenance = maintenance;
    }

    public double getMaintenance() {
        return maintenance;
    }

    public double getTotalBuildingRent() {
        double totalRent = 0.0;

        for (Apartment apartment : apartments) {
            totalRent += apartment.getRent();
        }

        return totalRent;
    }

    public double getProfit() {
        return getTotalBuildingRent() - maintenance;
    }
}
