package inheritance;

public class Main {
    public static void main(String[] args){
        Cylinder cylinder = new Cylinder(2, 5);
        System.out.println("Volume of Cylinder : "+cylinder.computeVolume());
    }
}
