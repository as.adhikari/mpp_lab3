package composition;

public class Cylinder {
    private double height;
    private Circle circle;

    Cylinder(double radius, double height) {
        circle = new Circle(radius);
        this.height = height;
    }

    public double getHeight() {

        return height;
    }

    public double computeVolume() {
        return circle.computeArea() * height;
    }
}
