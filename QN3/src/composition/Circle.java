package composition;

public class Circle {
    private double radius;

    public double getRadius() {
        return radius;
    }

    public double computeArea() {
        return Math.PI * radius * radius;
    }

    Circle(double r) {
        radius = r;
    }
}
