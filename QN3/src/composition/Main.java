package composition;

public class Main {

    public static void main(String[] args) {
        Circle c1 = new Circle(2);
        Cylinder c2 = new Cylinder(2, 5);

        System.out.println("Circle area = " + c1.computeArea());
        System.out.println("Cylinder volume = " + c2.computeVolume());
    }
}
