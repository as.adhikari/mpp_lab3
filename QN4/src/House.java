public class House extends Property {
    private double lotSize;

    House(double rent, double lotSize) {
        super(rent);
        this.lotSize = lotSize;
    }

    @Override
    double computeRent() {
        return this.getRent() * lotSize;
    }
}
