public class Trailer extends Property {
    Trailer(double rent) {
        super(rent);
    }

    @Override
    double computeRent() {
        return this.getRent();
    }
}
