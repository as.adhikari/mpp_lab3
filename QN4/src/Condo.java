public class Condo extends Property {
    private int numFloors;

    Condo(double rent, int numFloors) {
        super(rent);
        this.numFloors = numFloors;
    }

    @Override
    double computeRent() {
        return this.getRent() * numFloors;
    }
}
