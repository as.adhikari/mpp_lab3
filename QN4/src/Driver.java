public class Driver {

    public static void main(String[] args) {

        Property[] properties = { new House(0.1,9000), new Condo(400,2), new Trailer(500) };

        double totalRent = Admin.computeTotalRent(properties);
        
        System.out.println(totalRent);
    }
}
