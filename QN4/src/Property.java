public abstract class Property {
    private double rent;
    private Address address;

    Property(double rent){
        this.rent = rent;
    }

    Property(double rent, Address address){
        this.rent = rent;
        this.address = address;
    }

    Property(double rent, String street, String city, String state, int zip){
        this.rent = rent;
        this.address = new Address(street,city,state,zip);
    }

    public double getRent(){
        return rent;
    }

   abstract double computeRent();
}
